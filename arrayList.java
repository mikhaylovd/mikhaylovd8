public class arrayList<T>{
    private final int INIT_SIZE = 10;
    private int pointer = 0;
    private Object[] array = new Object[INIT_SIZE];
    public int size(){
        return pointer;
    }
    public boolean isEmpty(){
        if (pointer == 0) return true;
        else return false;
    }
    public T get(int index){
        return (T) array[index];
    }
    public void add(T obj){
        if (pointer == array.length -1) resize(array.length*2);
        array[pointer++] = obj;
    }
    public boolean removeIndex(int index){
        if (index >= pointer || index < 0) return false;
        pointer--;
        for (int i = index; i < pointer; i++){
            array[i] = array[i+1];
        }
        return true;
    }
    public boolean remove(T obj){
        return removeIndex(contains(obj));
    }
    public int contains(T obj){
        for (int i = 0; i < pointer; i++){
            if (array[i].equals(obj)){
                return i;
            }
        }
        return -1;
    }
    public void clear(){
        while (!isEmpty()){
            removeIndex(pointer -1);
        }
    }
    public String toString(){
        String tempString = "";
        for (int i = 0; i < pointer; i++){
            tempString += array[i] + " ";
        }
        return tempString;
    }
    private void resize(int length){
        Object[] tempArray = new Object[length];
        System.arraycopy(array,0,tempArray, 0, pointer);
        array = tempArray;
    }
}
