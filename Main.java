import java.util.Iterator;

class hashSet <T>{
    arrayList<T>[] arr = new arrayList[32];
    hashSet() {
        for (int i = 0; i < 32; i++) {
            arr[i] = new arrayList<T>();
        }
    }
    private int hashcode(T item){
        return System.identityHashCode(item) % 32;
    }
    public boolean isEmpty(){
        for (int i = 0; i < 32; i++){
            if (!arr[i].isEmpty()) return false;
        }
        return true;
    }
    public boolean contains(T item){
        if (arr[hashcode(item)].contains(item) != -1) return true;
        else return false;
    }
    public void add(T item){
        if (!contains(item)) arr[hashcode(item)].add(item);
    }
    public String toString(){
        String s = "";
        for (int i = 0 ; i < 32; i++){
            if (!arr[i].isEmpty()){
                s += arr[i].toString();
            }
        }
        return s;
    }
    public int size(){
        int size = 0;
        for (int i = 0 ; i < 32; i++){
            size += arr[i].size();
        }
        return size;
    }
    public void remove(T item){
        arr[hashcode(item)].remove(item);
    }
    public Iterator<T> iterator(){
        return new Iterator<T>() {
            private int count = 0;
            private int lastArr = 0;
            private int lastObj = 0;
            public boolean hasNext() {
                return count < size();
            }
            public T next() {
                while (true) {
                    if (lastObj == arr[lastArr].size()) {
                        lastObj = 0;
                        lastArr += 1;
                    }
                    else break;
                }
                count += 1;
                return arr[lastArr].get(lastObj++);
            }
        };
    }

}
public class Main {
    public static void main(String[] args) {
        hashSet<String> a = new hashSet<>();
    }
}